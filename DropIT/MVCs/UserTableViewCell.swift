//
//  UserTableViewCell.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    private let sidePadding: CGFloat = 16.0

    @IBOutlet private weak var imageViewRight: UIImageView!
    @IBOutlet private weak var imageViewLeft: UIImageView!
    @IBOutlet weak var stackViewHeightConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
    func updateCell(with data: (left: String, right: String?))
    {
        var width: CGFloat = 0

        setImageWith(urlSrting: data.left, on: imageViewLeft)
        if let rightUrlString = data.right{
            imageViewRight.isHidden = false
            setImageWith(urlSrting: rightUrlString, on: imageViewRight)
            width = (contentView.bounds.size.width - 3 * sidePadding)/2.0
        }
        else{
            imageViewRight.isHidden = true
            width = contentView.bounds.size.width - 2 * sidePadding
        }
        stackViewHeightConstraint.constant = width
        self.layoutIfNeeded()
        self.setNeedsLayout()
    }
    
    private func setImageWith(urlSrting: String, on imageView: UIImageView){
        imageView.image = nil
        guard let url = URL(string: urlSrting) else{
            return
        }
        
        imageView.af_setImage(
            withURL: url,
            placeholderImage: nil,
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
}

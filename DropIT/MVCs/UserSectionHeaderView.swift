//
//  UserSectionHeaderView.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit
import AlamofireImage

class UserSectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()        
        guard let imageView = self.imageView else { return }
        imageView.layer.cornerRadius = imageView.frame.height/2.0
    }
    
    func updateView(with name: String, urlString: String){
        titleLabel.text = name
        
        imageView.image = nil
        guard let url = URL(string: urlString) else{
            return
        }
        
        imageView.af_setImage(
            withURL: url,
            placeholderImage: nil,
            filter: nil,
            imageTransition: .crossDissolve(0.2)
        )
    }
}

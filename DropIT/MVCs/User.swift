//
//  User.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import Foundation

class Users {
    private(set) var users = [User]()
    let hasMore: Bool
    var offSet: Int = -1
    
    init(data: [String : Any]) {
        hasMore = data["has_more"] as? Bool ?? false
        if let users = data["users"] as? [[String: Any]]{
            self.users = users.map({ (dict) -> User in return User(data: dict) })
        }
    }
    
    func insertPreviousUsers(_ previousUsers: Users) -> Bool{
        if previousUsers.offSet < self.offSet{
            self.users = previousUsers.users + self.users
            return true
        }
        return false
    }
}

class User {
    let name: String
    let image: String
    let items: [String]
    
    init(data: [String : Any]) {
        name = data["name"] as? String ?? "UnNamed"
        image = data["image"] as? String ?? ""
        items = data["items"] as? [String] ?? []
    }
}

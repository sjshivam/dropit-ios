//
//  UserViewController.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit

class UserViewController: DRTableViewController {

    private let reuserableIdentifierHeaderView = "UserSectionHeaderView"
    private var offSet = 0
    private let limit = 10
    private var allUsers = Users(data: [:])
    

    override func viewDidLoad() {
        super.viewDidLoad()

        let nib = UINib(nibName: reuserableIdentifierHeaderView, bundle: nil)
        tableView.register(nib, forHeaderFooterViewReuseIdentifier: reuserableIdentifierHeaderView)
        tableView.estimatedRowHeight = 44
    }
    
    override func downloadData() {
        beginRefeshing()
        NetworkManager.shared().fetchUsers(offSet: offSet, limit: limit) { [weak self] (users, error) in
            self?.processData(users, error: error)
        }
    }
    
    private func processData(_ users: Users?, error: Error?){
        endRefeshing()
        guard let users = users else {
            print(error?.localizedDescription ?? "")
            showAlert(message: error?.localizedDescription)
            return
        }
        
        let success = users.insertPreviousUsers(allUsers)
        allUsers = success ? users : allUsers
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return allUsers.users.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let users = allUsers.users
        let rows = ceil(Double(users[section].items.count)/2.0)
        return Int(rows)
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as? UserTableViewCell else{
            fatalError("UserTableViewCell not found!!")
        }
        
        let users = allUsers.users
        let user = users[indexPath.section]
        if user.items.isOdd && tableView.isFirstCell(for: indexPath){
            cell.updateCell(with: (left: user.items[0], right: nil))
        }
        else{
            let index = indexPath.row * 2
            let offSet = user.items.isEven ? 0 : -1
            let (leftImage, rightImage) = (user.items[index + offSet], user.items[index + offSet + 1])
            cell.updateCell(with: (left: leftImage, right: rightImage))
        }
        
        return cell
    }

    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        guard let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: reuserableIdentifierHeaderView) as? UserSectionHeaderView else { return nil }
        
        let users = allUsers.users
        let user = users[section]
        header.updateView(with: user.name, urlString: user.image)
        
        return header
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 72
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if shouldLoadNextPage(for: indexPath) {
            showFooterLoader()
            offSet += limit
            downloadData()
        }else{
            hideFooterLoader()
        }
    }
    
    func shouldLoadNextPage(for indexPath: IndexPath) -> Bool {
        let loadMore = tableView.isLastCell(for: indexPath) && allUsers.hasMore
        return loadMore
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}

extension UserViewController{
    
    private func showAlert(message: String?) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        let confirmAction = UIAlertAction(title: "Okay", style: .default) {(_) in }
        alert.addAction(confirmAction)
        present(alert, animated: true, completion: nil)
    }
}

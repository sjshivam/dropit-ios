//
//  DRTableViewController.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit

class DRTableViewController: UITableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        downloadData()
    }

    @objc func downloadData(){
        
    }
    
    func beginRefeshing(){
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(downloadData), for: .valueChanged)
        tableView.refreshControl = refreshControl
        refreshControl.beginRefreshing()
    }
    
    func endRefeshing(){
        tableView.refreshControl?.endRefreshing()
    }
    
    private func loader() -> UIView {
        let spinner = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        spinner.color = UIColor.lightGray
        spinner.startAnimating()
        return spinner
    }
    
    func showFooterLoader() {
        tableView.tableFooterView = loader()
    }
    
    func hideFooterLoader(){
        tableView.tableFooterView = nil
    }
    
    // MARK: - Table view data source
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 0
    }

}

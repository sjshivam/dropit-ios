//
//  Extentions.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit

extension UITableView{
    
    func isFirstCell(for indexpath: IndexPath) -> Bool {
        return (indexpath.row == 0)
    }
    
    func isLastCell(for indexpath: IndexPath) -> Bool {
        let isLastSection = numberOfSections - 1 == indexpath.section
        let isLastRow = isLastSection && ((numberOfRows(inSection: indexpath.section) - 1 ) == indexpath.row)
        return isLastRow
    }
    
    func updateHeight(){
        self.beginUpdates()
        self.endUpdates()
    }
}

extension Array{
    var isEven: Bool { return self.count % 2 == 0 }
    var isOdd: Bool { return !isEven }
}

//
//  NetworkManager.swift
//  DropIT
//
//  Created by A14Z8R5R on 24/11/18.
//  Copyright © 2018 DropIT. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator

struct EndPoint {
    static let getUsers = "/api/users"
}

class NetworkManager {
    
    private static let sharedNetworkManager: NetworkManager = {
        NetworkActivityIndicatorManager.shared.isEnabled = true
        let networkManager = NetworkManager()
        return networkManager
    }()
    
    class func shared() -> NetworkManager {
        return sharedNetworkManager
    }
    
    // MARK: - Properties
    private static let baseURL = "http://sd2-hiring.herokuapp.com/"
    
    
    // With Alamofire
    func fetchUsers(offSet: Int, limit: Int = 10, completion: @escaping (Users?, Error?) -> Void) {
        guard let url = URL(string: NetworkManager.baseURL + EndPoint.getUsers ) else {
            completion(nil, FLError.defaultError)
            return
        }
        
        Alamofire.request(url,
                          method: .get,
                          parameters: ["limit": limit,
                                       "offset" : offSet])
            .validate()
            .responseJSON { response in
                guard response.result.isSuccess else {
                    completion(nil, response.result.error)
                    return
                }
                
                guard let value = response.result.value as? [String: Any],
                    let dataDict = value["data"] as? [String: Any] else{
                        completion(nil, FLError.defaultError)
                        return
                }
                print(dataDict)
                let users = Users.init(data: dataDict)
                users.offSet = offSet
                completion(users, nil)
        }
    }
}

public enum FLError: Error {
    case defaultError
}

extension FLError: LocalizedError {
    public var errorDescription: String? {
        switch self {
        case .defaultError:
            return NSLocalizedString("Oops! Something went wrong!!", comment: "")
        }
    }
}

